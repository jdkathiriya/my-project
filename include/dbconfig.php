<?php	
define('NO_IMAGE','download.png');
	date_default_timezone_set("Asia/Kolkata");
	class connection
{
		//private $con;
		var $con;
		public function __construct()
		{
			$this->con=mysqli_connect("localhost","root","","manage_employee");
		}
		function insertRec($data,$TblName)
		{
			$qry="";
			$col="";
			$val="";
			$cnt=count($data);
			$i=1;
			foreach($data as $k=>$v)
			{
				if($i<$cnt)
				{
					$col.=$k.",";
					$val.="'".mysqli_real_escape_string($this->con,$v)."',";
				}
				else
				{
					$col.=$k;
					$val.="'".mysqli_real_escape_string($this->con,$v)."'";
				}
				$i++;
			}
			$qry= "insert into ".$TblName." (".$col.") values (".$val.")";
			// echo $qry;
			// die;
			$res=mysqli_query($this->con,$qry);
			return $res;
		}
		function updateRec($dataArr,$conditionArr,$TblName)
		{
			$qry="";
			$condition="";
			$val="";
			$cnt=count($dataArr);
			$i=1;
			//Value Data
			foreach($dataArr as $k=>$v)
			{
				if($i<$cnt)
				{
					$val.=$k."='".mysqli_real_escape_string($this->con,$v)."',";
				}
				else
				{
					$val.=$k."='".mysqli_real_escape_string($this->con,$v)."'";
				}
				$i++;
			}
			//Condition Data
			$cnt=count($conditionArr);
			$i=1;
			foreach($conditionArr as $k=>$v)
			{
				if($i<$cnt)
				{
					$condition.=$k."='".mysqli_real_escape_string($this->con,$v)."' AND ";
				}
				else
				{
					$condition.=$k."='".mysqli_real_escape_string($this->con,$v)."'";
				}
				$i++;
			}

			$Updateqry= "UPDATE ".$TblName."  SET ".$val;
			if($cnt>0)
			{
				$Updateqry.= "  WHERE ".$condition;
			}
			//echo $Updateqry;
			$res=mysqli_query($this->con,$Updateqry);
			return $res;
		}
		//count row query
		function countrow($str)
		{
			$res=mysqli_query($this->con,$str);
			return mysqli_num_rows($res);
			
		}
		//get rows
		function getrows($str)
		{
			$res=mysqli_query($this->con,$str);
			return $res;
		}
		//function update/delete rows
		function updatedeleterows($u_d_qry)
		{
			$res=mysqli_query($this->con,$u_d_qry);
			return $res;
		}
	function validation($post)
	{
				$_POST = $post;
				$error_array = array();
				if (isset($_POST['first_name']) && (ctype_alpha($_POST['first_name']) == '' )) {
				$error_array['fn']= "Invalid FirstName"; 		
			   }
			   if (isset($_POST['last_name']) && (ctype_alpha($_POST['last_name']) == '' )) {
				$error_array['ln']= "Invalid LastName"; 
			   }
			   if(isset($_POST['address']) && $_POST['address'] == ''){
				   $error_array['ad']= "Invalid Address";
			   }
			   if(isset($_POST['date_of_birth']) && $_POST['date_of_birth'] == ''){
			   	 $error_array['bd']= "Invalid Birthdate";
			   }
			    if(!isset($_POST['gender'])){
	   			$error_array['gn']= "Invalid gender";
				}
			   if(isset($_POST['mobile_no']) && (ctype_digit($_POST['mobile_no']) == '' )){  
				   $error_array['mn']= "Invalid MobileNo";
			   }
			 if(!isset($_POST['id'])){
				$email=$_POST['email'];
			   $checkemaildata=$this->getrows("SELECT *  from registration_master where email= '$email' ");
			   $cud=mysqli_fetch_assoc($checkemaildata);
			   if (!empty($_POST['email'])){
				   $email = $_POST['email'];
				   // check if e-mail address is well-formed
				   if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
					   $error_array['em']= "Invalid email format";
				   }elseif($email == $cud["email"]){
					 $error_array['em'] = "Email ALreadry exist";
				   }
				 } else {
				   $error_array['em'] = "Email is required";
				 }
			 }else{
				 if (isset($_POST['email'])){
				 	$email=$_POST['email'];
				 if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
					   $error_array['em']= "Invalid email format";
				   }
				}
				  else {
				   $error_array['em'] = "Email is required";
				 }

			} 
				if(isset($_POST['password']) && $_POST['password'] == ''){
				    $error_array['pa']= "Invalid Password";
			   }
			   if (!isset($_POST['confirm_password']) && isset($_POST['password']) && $_POST['confirm_password'] !=  $_POST['password']) 
			   {
				    $error_array['cp'] = "Password and Confirm Password are not Matched.";
			   }
			   if(isset($_POST['joining_date']) && $_POST['joining_date'] == ''){
				
				  $error_array['jd']= "Invalid Joining Date";
			   }
			   return $error_array;
	}

	function insertRecord($post)
	{
				$return_array = $response_data= array();
				$error_array = $this->validation($post);
					$src=$_FILES['image']['tmp_name'];
					$dest=$_FILES['image']['name'];
					$status=1;        
				if(!empty($error_array)){
					$response_data['dataResult'] = "fail";
					$response_data['errors'] = $error_array; 
				}else{
					
					$imageFileType = strtolower(pathinfo($dest,PATHINFO_EXTENSION));
				 
					if($imageFileType == "jpg" || $imageFileType == "jpeg" || $imageFileType ==  "png")
						{
							move_uploaded_file($src,"images/".$dest);
							if($_POST['method']=="insert"){
								$response_data['dataResult'] = "Success";
								 $insertprofileArr = array('first_name' => $_POST['first_name'],'last_name' => $_POST['last_name'],'date_of_birth' => $_POST['date_of_birth'],
																	'gender' => $_POST['gender'],'address' => $_POST['address'],
																	'mobile_no' => $_POST['mobile_no'],'email' => $_POST['email'],
																	'image'=>$dest,'password' => $_POST['password'],'confirm_password' =>$_POST['confirm_password'],
																	'joining_date'=>$_POST['joining_date'],'status'=>$status);
								$insertProfileData = $this -> insertRec($insertprofileArr,'registration_master');   
				
							}
						}
						
						else{
							$response_data['dataResult'] = "fail";
							$response_data['errors']['im'] = "Invalid image formate";
							}               
			  }
			  return json_encode($response_data); 
	}			
	function searchRecord($post)
	{
					$i=1;
					$limit = '5';
					$page = 1;
					$output='';
					
					if($_POST['page'] > 1)
					{
					$start = (($_POST['page'] - 1) * $limit);
					$i= (($_POST['page'] - 1)  * $limit) + 1 ;
					$page = $_POST['page'];
					}
					else
					{
					$start = 0;
					}
					$query = "
					SELECT * FROM registration_master where  role=1 
					";
					
				
					if($_POST['query'] != '')
					{
					$query .= '
					AND first_name LIKE "%'.str_replace(' ', '%', $_POST['query']).'%" 
					';
					
					}
					$total_data=$this->countrow($query);

				
					$output = '
					<table class="table"  >
					<thead>
					<tr>
					<th></th>
					<th class="serial">#</th>
					<th class="avatar">Profile</th>
					<th>Name</th>
					<th>Email</th>
					<th>Address</th>
					<th>Mobile No</th>
					<th>Status</th>
					<th>Action</th>
					</tr>
					';
					if($total_data > 0)
					{ 
					$total_filter_data=$this->getrows($query . 'LIMIT '.$start.', '.$limit.'');
					while($row=mysqli_fetch_assoc($total_filter_data)){
						$image = (file_exists('images/'.$row['image'])) ? 'images/'.$row['image'] : NO_IMAGE;
					
							$status=$row['status'];
							if($status == 1){
							$status = '<button  class="btn btn-success btn-sm updatesatus" data-id='.$row['id'].'> Active </button>';
							}else{
							$status = '<button class="btn btn-danger btn-sm updatesatus" data-id='.$row['id'].'>InActive</button> ';
							}
				
						$output .= '
						<tr id=tr_'. $row['id'].'>
						<td><input type="checkbox" id="del_'. $row['id'] .'"></td>
						<td>'.$i++.'</td>
						<td><img src='. $image.' height="50" width="100" alt="default.png"></td>
						<td>'.$row['first_name'].'</td>
						<td>'.$row['email'].'</td>
						<td>'.$row['address'].'</td>
						<td>'.$row['mobile_no'].'</td>
						<td>'.$status.'</td>
						<td>
						<span class="delete" data-id='.$row['id'].'>
							<button  class="btn btn-raised btn-default m-t-15 waves-effect"><i class="fa fa-trash"></i>
							</button>
							</span>
							<span class="edit" data-id='.$row['id'].'>
							<button  class="btn btn-raised btn-default m-t-15 waves-effect" data-toggle="modal" data-target="#modalForm"><i class="fa fa-edit" ></i>
							</button>
							</span>
						</td>
						</tr>
						';
					}
					}
					else
					{
					$output .= '
					<tr>
						<td colspan="2" align="center">No Data Found</td>
					</tr>
					';
					}
					
					$output .= '
					</thead>
				<tbody>
				</tbody>
					</table>
					<br />
					<div align="center">
					<ul class="pagination">
					';
					
					$total_links = ceil($total_data/$limit);
					$previous_link = '';
					$next_link = '';
					$page_link = '';
					$page_array=array();
					
					if($total_links > 4)
					{
					if($page < 5)
					{
						for($count = 1; $count <= 5; $count++)
						{
						$page_array[] = $count;
						}
						$page_array[] = '...';
						$page_array[] = $total_links;
					}
					else
					{
						$end_limit = $total_links - 5;
						if($page > $end_limit)
						{
						$page_array[] = 1;
						$page_array[] = '...';
						for($count = $end_limit; $count <= $total_links; $count++)
						{
							$page_array[] = $count;
						}
						}
						else
						{
						$page_array[] = 1;
						$page_array[] = '...';
						for($count = $page - 1; $count <= $page + 1; $count++)
						{
							$page_array[] = $count;
						}
						$page_array[] = '...';
						$page_array[] = $total_links;
						}
					}
					}
					else
					{
					for($count = 1; $count <= $total_links; $count++)
					{
						$page_array[] = $count;
					}
					}
					
					for($count = 0; $count < count($page_array); $count++)
					{
					if($page == $page_array[$count])
					{
						$page_link .= '
						<li class="page-item active">
						<a class="page-link" href="#">'.$page_array[$count].' <span class="sr-only">(current)</span></a>
						</li>
						';
					
						$previous_id = $page_array[$count] - 1;
						if($previous_id > 0)
						{
						$previous_link = '<li class="page-item"><a class="page-link" href="javascript:void(0)" data-page_number="'.$previous_id.'">Previous</a></li>';
						}
						else
						{
						$previous_link = '
						<li class="page-item disabled">
							<a class="page-link" href="#">Previous</a>
						</li>
						';
						}
						$next_id = $page_array[$count] + 1;
						if($next_id >= $total_links)
						{
						$next_link = '
						<li class="page-item disabled">
							<a class="page-link" href="#">Next</a>
						</li>
							';
						}
						else
						{
						$next_link = '<li class="page-item"><a class="page-link" href="javascript:void(0)" data-page_number="'.$next_id.'">Next</a></li>';
						}
					}
					else
					{
						if($page_array[$count] == '...')
						{
						$page_link .= '
						<li class="page-item disabled">
							<a class="page-link" href="#">...</a>
						</li>
						';
						}
						else
						{
						$page_link .= '
						<li class="page-item"><a class="page-link" href="javascript:void(0)" data-page_number="'.$page_array[$count].'">'.$page_array[$count].'</a></li>
						';
						}
					}
					}
					$output .= $previous_link . $page_link . $next_link;
					$output .= '
					</ul>
					</div>
					';
					return json_encode(array("output" => $output));
	}
	
	function updateRecord($post)
	{
		$response_data= array();
			$error_array=$this->validation($post);
			
                $src=$_FILES['image']['tmp_name'];
                $dest=$_FILES['image']['name'];
				$status=1;  
				if(!empty($error_array)){
					$response_data['dataResult'] = "fail";
					$response_data['errors'] = $error_array; 
				}else{ 
					if($dest != ""){					
					  $imageFileType = strtolower(pathinfo($dest,PATHINFO_EXTENSION));		
					  if( $imageFileType == "jpg" || $imageFileType == "jpeg" || $imageFileType == "png")
						  {   
							  move_uploaded_file($src,"images/".$dest);
							  if($_POST['method']=="update"){
									$response_data['dataResult'] = "Success";
									$id=$_POST['id'];
									$Updateqry = "UPDATE   registration_master  set  first_name = '".$_POST['first_name']."',last_name ='".$_POST['last_name']."',
									date_of_birth = '".$_POST['date_of_birth']."',gender = '".$_POST['gender']."',address ='".$_POST['address']."', mobile_no = '".$_POST['mobile_no']."',
									email = '".$_POST['email']."',image= '$dest',password = '".$_POST['password']."', confirm_password = '".$_POST['confirm_password']."',
									joining_date ='".$_POST['joining_date']."',status = '$status' where id='$id'";  
									$res = $this -> updatedeleterows($Updateqry);   
							
								}
							} else{
							  $response_data['dataResult'] = "fail";
							  $response_data['errors']['im'] = "Invalid image formate";
							}               
						}else{
							$response_data['dataResult'] = "Success";
							  $id=$_POST['id'];
							  $Updateqry = "UPDATE   registration_master  set  first_name = '".$_POST['first_name']."',last_name ='".$_POST['last_name']."',
							  date_of_birth = '".$_POST['date_of_birth']."',gender = '".$_POST['gender']."',address ='".$_POST['address']."', mobile_no = '".$_POST['mobile_no']."',
							  email = '".$_POST['email']."',password = '".$_POST['password']."', confirm_password = '".$_POST['confirm_password']."',
							  joining_date ='".$_POST['joining_date']."',status = '$status' where id='$id'";                  
							  $res = $this -> updatedeleterows($Updateqry); 
						  }
						}

    
                    return json_encode($response_data);  
	}	
	function loginRecord($post)
	{
		$email=isset($_POST['email']) ? $_POST['email'] : '';
		$password=isset($_POST['password']) ? $_POST['password'] : '';
		
		$checkUser=$this->countrow("SELECT * from registration_master");
		
		if($checkUser>0)
		{
			$UserData=$this->getrows("SELECT *  from registration_master where role = '0' AND email= '$email' AND  password= '$password' ");
			$ud=mysqli_fetch_assoc($UserData);
			
			if(isset($ud['email']) && $email == $ud['email'] && $password == $ud['password']) {
			$_SESSION['first_name']=$ud['first_name'];
			$_SESSION['last_name']=$ud['last_name'];
			$_SESSION['email']=$ud['email'];
			$_SESSION['image']=$ud['image'];  
			$_SESSION['id']=$ud['id'];
			
					return json_encode(array("status_Code"=>200));
					//return $mydata;
				}
				else{
					return json_encode(array("status_Code"=>201));
					//return $mydata;
				}
			}
		else{
				return json_encode(array("status_Code"=>201));
				//return $mydata;
		}  
	}
	function multideleteRecord($post)
	{
		$id = $_POST['id'];

		foreach($id as $id){ 
				$u_d_qry="delete  from registration_master where id=".$id;
				$updatedeleterows=$cnn ->updatedeleterows($u_d_qry,'registration_master');
				}
				return json_encode(array("status_Code"=>200));
	}
	function deleteRecord($post)
	{
		$id=$_POST['id'];
          $u_d_qry="delete  from registration_master where id='$id' ";
          $updatedeleterows=$this ->updatedeleterows($u_d_qry,'registration_master');
          return json_encode(array("statusCode"=>200));
	}
	function editRecord($post)
	{
		$id=$_POST['id'];
		$empuser=$this->getrows("SELECT * from registration_master where id='$id'");
		$row = mysqli_fetch_assoc($empuser);  
		return json_encode($row); 
	}
	function updatestatusRecord($post)
	{
		$status='';
		$id=$_POST['id'];
		$empuser=$this->getrows("SELECT * from registration_master where id='$id'"); 
		$row = mysqli_fetch_assoc($empuser); 
		$status = $row['status']; 
		if($status == 1)
		{
			$status=0;
		}
		else
		{
			$status=1;
		}
			$Updateqry = "UPDATE   registration_master  set   status = '$status' where id='$id'";
			// echo "<pre>";
			// print_r($Updateqry);
			// die;
			$res = $this -> updatedeleterows($Updateqry);  
			return json_encode(array("status_Code"=>200,"status" => $status));
	}


}
?>
$(document).ready(function() {
    $("#submitLogin").click(function(event) {
        event.preventDefault();
        var email = $("#email").val();
        var password = $("#password").val();

        if (email != "" && password != "") {
            $.ajax({
                url: "responce.php",
                type: "POST",
                data: { method: "login", email: email, password: password },
                success: function(dataResult) {
                    // console.log(dataResult);
                    // return false;
                    var dataResult = JSON.parse(dataResult);

                    if (dataResult.status_Code == 200) {
                        $("#error").show();
                        $("#error").html('Login Successfully !!!');
                        location.href = "index.php";
                    } else {
                        $("#error").show();
                        $("#error").html(
                            "Invalid EmailId or Password !!!"
                        );
                    }
                },
            });
        } else {
            $("#error").show();
            $("#error").text("Please Fill  Email or Password !!!");
        }
    });
});
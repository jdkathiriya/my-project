$(document).ready(function() {
    $(".addEmployee").click(function() {
        $(".employeeListing").hide();
        $(".registerForm").show();
    });
    $(".ViewEmployee").click(function() {
        $(".registerForm").hide();
        $(".employeeListing").show();
    });
    $('#submit').click(function(event) {
        event.preventDefault();
        var form_data = $("#frm")[0];
        var form_data = new FormData(form_data);
        form_data.append("method", "insert");
        $.ajax({
            url: "responce.php",
            type: "post",
            data: form_data,
            processData: false,
            contentType: false,
            success: function(dataResult) {
                // console.log(dataResult);
                // return false;
                var dataResult = JSON.parse(dataResult);

                if (dataResult.dataResult == "fail") {
                    // console.log(dataResult.dataResult);
                    // return false;
                    dataResult["errors"]["fn"] !== undefined ?
                        $(".demofn").html(dataResult["errors"]["fn"]) :
                        $(".demofn").html("");
                    dataResult["errors"]["ln"] !== undefined ?
                        $(".demoln").html(dataResult["errors"]["ln"]) :
                        $(".demoln").html("");
                    dataResult["errors"]["bd"] !== undefined ?
                        $(".demobd").html(dataResult["errors"]["bd"]) :
                        $(".demobd").html("");
                    dataResult["errors"]["ad"] !== undefined ?
                        $(".demoad").html(dataResult["errors"]["ad"]) :
                        $(".demoad").html("");
                    dataResult["errors"]["gn"] !== undefined ?
                        $(".demogn").html(dataResult["errors"]["gn"]) :
                        $(".demogn").html("");
                    dataResult["errors"]["im"] !== undefined ?
                        $(".demoim").html(dataResult["errors"]["im"]) :
                        $(".demoim").html("");
                    dataResult["errors"]["mn"] !== undefined ?
                        $(".demomn").html(dataResult["errors"]["mn"]) :
                        $(".demomn").html("");
                    dataResult["errors"]["cp"] !== undefined ?
                        $(".democp").html(dataResult["errors"]["cp"]) :
                        $(".democp").html("");
                    dataResult["errors"]["em"] !== undefined ?
                        $(".demoem").html(dataResult["errors"]["em"]) :
                        $(".demoem").html("");
                    dataResult["errors"]["pa"] !== undefined ?
                        $(".demopa").html(dataResult["errors"]["pa"]) :
                        $(".demopa").html("");
                    dataResult["errors"]["jd"] !== undefined ?
                        $(".demojd").html(dataResult["errors"]["jd"]) :
                        $(".demojd").html("");
                } else {
                    $(".demofn").html("");
                    $(".demoln").html("");
                    $(".demobd").html("");
                    $(".demoad").html("");
                    $(".demogn").html("");
                    $(".demoim").html("");
                    $(".demomn").html("");
                    $(".democp").html("");
                    $(".demoem").html("");
                    $(".demopa").html("");
                    $(".demojd").html("");
                    $("#frm")[0].reset();
                    load_data(1, 1);
                }
            },
        });
    });
    $('.changeProfile').click(function() {

        $('.callFor').val('1');
        //  $('.nameChange').val('1');
    });
    $("#update").click(function(event) {
        event.preventDefault();
        var form_data = $("#fupForm")[0];
        var form_data = new FormData(form_data);
        form_data.append("method", "update");
        $.ajax({
            url: "responce.php",
            type: "post",
            data: form_data,
            processData: false,
            contentType: false,
            success: function(dataResult) {
                var dataResult = JSON.parse(dataResult);
                if (dataResult.dataResult == "fail") {
                    dataResult["errors"]["fn"] !== undefined ?
                        $(".demofn1").html(dataResult["errors"]["fn"]) :
                        $(".demofn1").html("");
                    dataResult["errors"]["ln"] !== undefined ?
                        $(".demoln1").html(dataResult["errors"]["ln"]) :
                        $(".demoln1").html("");
                    dataResult["errors"]["bd"] !== undefined ?
                        $(".demobd1").html(dataResult["errors"]["bd"]) :
                        $(".demobd1").html("");
                    dataResult["errors"]["ad"] !== undefined ?
                        $(".demoad1").html(dataResult["errors"]["ad"]) :
                        $(".demoad1").html("");
                    dataResult["errors"]["rl"] !== undefined ?
                        $(".demorl1").html(dataResult["errors"]["rl"]) :
                        $(".demorl1").html("");
                    dataResult["errors"]["gn"] !== undefined ?
                        $(".demogn1").html(dataResult["errors"]["gn"]) :
                        $(".demogn1").html("");
                    dataResult["errors"]["mn"] !== undefined ?
                        $(".demomn1").html(dataResult["errors"]["mn"]) :
                        $(".demomn1").html("");
                    dataResult["errors"]["cp"] !== undefined ?
                        $(".democp1").html(dataResult["errors"]["cp"]) :
                        $(".democp1").html("");
                    dataResult["errors"]["em"] !== undefined ?
                        $(".demoem1").html(dataResult["errors"]["em"]) :
                        $(".demoem1").html("");
                    dataResult["errors"]["pa"] !== undefined ?
                        $(".demopa1").html(dataResult["errors"]["pa"]) :
                        $(".demopa1").html("");
                    dataResult["errors"]["jd"] !== undefined ?
                        $(".demojd1").html(dataResult["errors"]["jd"]) :
                        $(".demojd1").html("");
                    dataResult["errors"]["im"] !== undefined ?
                        $(".demoim1").html(dataResult["errors"]["im"]) :
                        $(".demoim1").html("");
                } else {
                    $(".demofn1").html("");
                    $(".demoln1").html("");
                    $(".demobd1").html("");
                    $(".demoad1").html("");
                    $(".demorl1").html("");
                    $(".demogn1").html("");
                    $(".demomn1").html("");
                    $(".democp1").html("");
                    $(".demoem1").html("");
                    $(".demojd1").html("");
                    $(".demopa1").html("");
                    $(".demoim1").html("");
                    $("#modalForm").modal("hide");
                    $('.modal-backdrop').remove();
                    $('.modal-show').hide();
                    //    success: function(response){
                    if ($('.callFor').val() == '1') {
                        // console.log($('.callFor').val());
                        // return false;
                        var img_src = $('.updateImage').attr('src');
                        jQuery('.profileImg').attr('src', img_src);
                        var namechange = $('#first_name1').val();
                        $('.NameChange').text(namechange);
                    }
                    load_data(2, 1);
                    //$(".employeeListing").show();
                }
            },
        });
    });
    load_data(0, 1);

    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(".updateImage").attr("src", e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#image1").change(function() {
        readURL(this);
    });

    function load_data(call, page, query = "") {
        $.ajax({
            url: "responce.php",
            type: "POST",
            data: { method: "search", page: page, query: query },
            success: function(dataResult) {
                var dataResult = JSON.parse(dataResult);
                $("#viewtable").html(dataResult["output"]);
                $(".registerForm").hide();
                $(".employeeListing").show();
                // $("#demoooo").modal("show");
                if (call == 1) {
                    // console.log(call);
                    // return false;
                    $(".clickModel").click();
                    $(".msg11").html("Success");
                    $(".msg22").html("Register successfully");
                } else if (call == 2) {
                    // console.log(call);
                    // return false;
                    $(".clickModel").click();
                    $(".msg11").html("Update");
                    $(".msg22").html("Update successfully");
                } else {
                    $(".msg11").html();
                    $(".msg22").html();
                }
            },
        });
    }

    $(document).on("click", ".page-link", function() {
        var page = $(this).data("page_number");
        var query = $("#search").val();
        load_data(0, page, query);
    });

    $("#search").keyup(function(event) {
        event.preventDefault();
        var query = $("#search").val();
        load_data(0, 1, query);
    });

    function updatedata(id) {
        $.ajax({
            url: "responce.php",
            type: "POST",
            data: { method: "edit", id: id },
            cache: false,
            success: function(dataResult) {
                var dataResult = JSON.parse(dataResult);
                checkIfFileLoaded(dataResult.image);
                $("#id1").val(dataResult.id);
                $("#first_name1").val(dataResult.first_name);
                $("#last_name1").val(dataResult.last_name);
                $("#date_of_birth1").val(dataResult.date_of_birth);
                // checkradio(dataResult.gender);
                if (dataResult.gender == "0") {
                    $("#gendermale1").val("0").prop("checked", "checked");
                } else {
                    $("#genderfemale1").val("1").prop("checked", "checked");
                }

                $("#address1").val(dataResult.address);
                $("#mobile_no1").val(dataResult.mobile_no);
                $("#email1").val(dataResult.email);
                $("#password1").val(dataResult.password);
                $("#joining_date1").val(dataResult.joining_date);
                $("#role1").val(dataResult.role);
                $("#confirm_password1").val(dataResult.confirm_password);
                $("#modalForm").modal("show");
            },
        });
    }
    $(document).on("click", ".edit", function() {
        var el = this;
        var id = $(this).data("id");
        updatedata(id);
    });

    $(document).on("click", ".delete", function(event) {
        event.preventDefault();

        var el = this;
        var id = $(this).data("id");
        var confirmalert = confirm("Are you sure?");
        if (confirmalert == true) {
            $.ajax({
                url: "responce.php",
                type: "POST",
                data: {
                    method: 'delete',
                    id: id,
                },
                success: function(dataResult) {
                    var dataResult = JSON.parse(dataResult);
                    if (dataResult.statusCode == 200) {
                        $(el).closest("tr").css("background", "tomato");
                        $(el)
                            .closest("tr")
                            .fadeOut(800, function() {
                                $(this).remove();
                            });
                    } else {
                        alert("something went wrong");
                    }
                },
            });
        }
    });
    $(document).on("click", ".updatesatus", function() {
        // alert("status udate");

        var el = this;
        var id = $(this).data("id");
        var status = $(this).hasClass("btn-success") ? "0" : "1";
        $.ajax({
            type: "POST",
            url: "responce.php",
            data: {
                method: "updatestatus",
                id: id,
                status: status,
            },
            success: function(dataResult) {
                var dataResult = JSON.parse(dataResult);
                if (dataResult.status_Code == 200) {
                    if (dataResult.status == 0) {
                        $(el).css("background", "red").text("InActive");
                    }
                    if (dataResult.status == 1) {
                        $(el).css("background", "green").text("Active");
                    }
                }
            },
        });
    });
    $("#delete").click(function() {
        var post_arr = [];

        // Get checked checkboxes
        $("#viewtable input[type=checkbox]").each(function() {
            if (jQuery(this).is(":checked")) {
                //alert("hi");
                var id = this.id;
                var splitid = id.split("_");
                var pid = splitid[1];

                post_arr.push(pid);
            }
        });
        if (post_arr.length > 0) {
            var isDelete = confirm("Do you really want to delete records?");
            if (isDelete == true) {
                // AJAX Request
                $.ajax({
                    url: "responce.php",
                    type: "POST",
                    data: { method: "multipledelete", id: post_arr },
                    success: function(response) {
                        $.each(post_arr, function(i, l) {
                            $("#tr_" + l).remove();
                        });
                        //load_data(0, 1);
                    },
                });
            }
        }
    });
});


function checkIfFileLoaded(fileName) {
    $.ajax({
        url: "images/" + fileName,
        type: "HEAD",
        error: function() {
            $(".updateImage").attr("src", "download.png");
        },
        success: function() {
            $(".updateImage").attr("src", "images/" + fileName);
        },
    });
}